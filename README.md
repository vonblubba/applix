Applix ruby developer test
--------------------------

Rake task is run with:
    rake export:csv["path/to/csv/files"]

ruby version: 2.3.0p0 
rails version: 4.2.5 
database: Mysql 5.5.54 


Test
----

Descrizione
Date le tabelle descritte più sotto, creare all’interno di un nuovo progetto Rails 4.2.8 un task Rake che crei due file CSV con le seguenti estrazioni:
 
tutti gli utenti (nome, cognome, email, main_school_code) con ruolo "teacher" che abbiano il campo main_school_code della tabella schools non vuoto. 
tutti gli utenti (nome, cognome, email, province) con ruolo "teacher" dei quali non è possibile ricavare il main_school_code, ma che hanno valorizzato il province della tabella schools.
 
NOTE
un utente può avere più ruoli e può essere associato a più scuole.
il primo CSV deve chiamarsi main_school_teachers_<timestamp>.csv
il secondo CSV deve chiamarsi with_province_teachers_<timestamp>.csv
il path dove salvare i CSV deve poter essere specificato da riga di comando.
non è permesso utilizzare “find_by_sql”, la query deve essere gestita utilizzando ActiveRecord.
pubblicare il codice su un repository git (a scelta tra bitbucket e github).
il database utilizzato è PostresSQL 9.4 (verificare la versione)
Tabelle
Lo schema presentato sotto è parziale e include solo i campi strettamente necessari per lo svolgimento del test. Vanno create anche le tabelle roles_users e schools_users. La tabella dei ruoli deve includere i valori elencati di seguito. Per la tabella users e schools si è liberi di inserire dei dati random.
 
      name       
-----------------
 student
 teacher
 principal
 parent
 publisher
 promoter
 editor
 bsmart
 admin
 demo
 publisher_admin
 admin-tutors
 tutor
(13 rows)
 
 
 
 
Table "public.users"
           Column            |            Type             |
-----------------------------+-----------------------------+-----------------------
 id                          | integer                     | not null autoincrement
 name                        | character varying(255)      |
 surname                     | character varying(255)      |
 email                       | character varying(255)      | not null
 created_at                  | timestamp without time zone | not null
 updated_at                  | timestamp without time zone | not null
Indexes:
    "users_pkey" PRIMARY KEY, btree (id)
    "index_users_on_email" UNIQUE, btree (email)
 
                                     Table "public.roles"
   Column   |            Type             |
------------+-----------------------------+--------------------------------------
 id         | integer                     | not null autoincrement
 name       | character varying(255)      |
 created_at | timestamp without time zone | not null
 updated_at | timestamp without time zone | not null
Indexes:
    "roles_pkey" PRIMARY KEY, btree (id)
 
                                        Table "public.schools"
      Column      |            Type             |
------------------+-----------------------------+--------------------------------
 id               | integer                     | not null autoincrement
 name             | character varying(255)      |
 province         | character varying(255)      |
 main_school_code | character varying(255)      |
 created_at       | timestamp without time zone | not null
 updated_at       | timestamp without time zone | not null
Indexes:
    "schools_pkey" PRIMARY KEY, btree (id)
