require 'csv'

namespace :export do
  desc "Export data to csv file"

  task :csv, [:out_path] => [:environment] do |t, args|  
    args.with_defaults(:out_path => './')
    if Dir.exists?(args[:out_path])
      # setting output path
      path = args[:out_path]
      if path[-1] != "/"
        path += "/"
      end
      
      #main_school_teachers_*.csv
      CSV.open(path + "main_school_teachers_" + Time.now.strftime('%Y%m%d%H%M%S%L').to_s + ".csv", "wb") do |csv|
        Role.find_by_name('teacher').users
        User.joins(:roles).joins(:schools)
          .where("roles.name" => "teacher")
          .where.not("schools.main_school_code" => [nil, ''])
          .select("users.name, users.surname, users.email, schools.main_school_code")
          .each do |user|
          #puts user.inspect
          csv << [user.name, user.surname, user.email, user.main_school_code]
        end
      end
      
      #with_province_teachers_*.csv
      CSV.open(path + "with_province_teachers_" + Time.now.strftime('%Y%m%d%H%M%S%L').to_s + ".csv", "wb") do |csv|
        Role.find_by_name('teacher').users
        User.joins(:roles).joins(:schools)
          .where("roles.name" => "teacher")
          .where("schools.main_school_code" => [nil, ''])
          .where.not("schools.province" => [nil, ''])
          .select("users.name, users.surname, users.email, schools.province")
          .each do |user|
          csv << [user.name, user.surname, user.email, user.province]
        end
      end
      
    end
  end
end