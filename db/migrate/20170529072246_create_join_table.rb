class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :roles, :users do |t|
      # t.index [:role_id, :user_id]
      # t.index [:user_id, :role_id]
    end
    
    create_join_table :schools, :users do |t|
      # t.index [:school_id, :school_id]
      # t.index [:user_id, :role_id]
    end
  end
end
