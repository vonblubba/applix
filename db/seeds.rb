# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.destroy_all
role1 = Role.create(name: 'student')
role2 = Role.create(name: 'teacher')
Role.create(name: 'principal')
Role.create(name: 'parent')
Role.create(name: 'publisher')
Role.create(name: 'promoter')
Role.create(name: 'editor')
Role.create(name: 'bsmart')
Role.create(name: 'admin')
Role.create(name: 'demo')
Role.create(name: 'publisher_admin')
Role.create(name: 'admin-tutors')
Role.create(name: 'tutor')

User.destroy_all
100.times do |index|
  u = User.create(name: Faker::Name.first_name,
                surname: Faker::Name.last_name,
                email: Faker::Internet.email)
  if (index.even?) 
    u.roles << role1
  else
    u.roles << role2
  end
  u.save
end

School.destroy_all
100.times do |index|
  s =School.create(name: Faker::Name.first_name,
                province: Faker::Address.country_code)
  if (s.id.even?)
    s.main_school_code = Faker::Code.ean
    s.save
  end
end

User.all.each do |user|
  offset = rand(School.count)
  user.schools << School.offset(offset).first
  user.save
end